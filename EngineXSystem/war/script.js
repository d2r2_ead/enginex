<script type="text/javascript">
        function init() {
                //Parameters are APIName,APIVersion,CallBack function,API Root 
                gapi.client.load('productendpoint', 'v1', null, 'http://localhost:8888/_ah/api');
                
                document.getElementById('listProduct').onclick = function() {
                    listProducts();
                  }
                
                document.getElementById('insertProduct').onclick = function() {
                        insertProduct();
                }
                
                document.getElementById('updateProduct').onclick = function() {
                        updateProduct();
                }
                
                document.getElementById('deleteProduct').onclick = function() {
                        deleteProduct();
                }
        }
        
        //List Products function that will execute the listProduct call
        function listProducts() {
                gapi.client.productendpoint.listProduct().execute(function(resp) {
                        if (!resp.code) {
                                resp.items = resp.items || [];
                                var result = "";
                                for (var i=0;i<resp.items.length;i++) {
                                        result = result+ resp.items[i].message + "..." + "<b>" + resp.items[i].engineCode + "</b>" + "[" + resp.items[i].id + "]" + "<br/>";
                                }
                                document.getElementById('listProductsResult').innerHTML = result;
                        }
                });
        }
        
        //Insert Product function
        function insertProduct() {
                //Validate the entries
                var _EngineCode = document.getElementById('txtEngineCode').value;
                var _CodeName = document.getElementById('txtCodeName').value;
                
                //Build the Request Object
                var requestData = {};
                requestData.engineCode = _EngineCode;
                requestData.message = _CodeName;
                gapi.client.productendpoint.insertProduct(requestData).execute(function(resp) {
                        if (!resp.code) {
                                //Just logging to console now, you can do your check here/display message
                                console.log(resp.id + ":" + resp.engineCode + ":" + resp.message);
                        }
                });
        }
        
        //Update Product function
        function updateProduct() {
                //Validate the entries
                var _ID = document.getElementById("editProductID").value;
                var _EngineCode = document.getElementById('editEngineCode').value;
                var _CodeName = document.getElementById('editCodeName').value;
                
                //Build the Request Object
                var requestData = {};
                requestData.id = _ID;
                requestData.engineCode = _EngineCode;
                requestData.message = _CodeName;
                gapi.client.productendpoint.updateProduct(requestData).execute(function(resp) {
                        if (!resp.code) {
                                //Just logging to console now, you can do your check here/display message
                                console.log(resp.id + ":" + resp.engineCode + ":" + resp.message);
                        }
                });
        }
        
        //Delete Product function
        function deleteProduct() {
                //Validate the entries
                var _ID = document.getElementById('ProductID').value;
                
                //Build the Request Object
                var requestData = {};
                requestData.id = _ID;
                console.log(requestData);
                gapi.client.productendpoint.removeProduct(requestData).execute(function(resp) {
                        //Just logging to console now, you can do your check here/display message
                        console.log(resp);
                });
        }
        
    </script>
    <script src="https://apis.google.com/js/client.js?onload=init"></script>
</body>
</html>