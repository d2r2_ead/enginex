package enginex.entities;

public class SalesManager extends Rolls{

	private String link="authentication/salesmgr.html";
	
	@Override
	public String toString() {
		
		return "Sales Manager";
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}
}
